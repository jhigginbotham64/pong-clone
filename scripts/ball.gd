extends KinematicBody2D

signal score(against_player)

var SPEED = settings.ball_speed
var MOVE_DIR = Vector2()

onready var ball_size = {
	"x": $"ball_shape".get_shape().get_extents().x * 2,
	"y": $"ball_shape".get_shape().get_extents().y * 2,
}

onready var sounds = {
	"paddle_bounce": $"paddle_bounce",
	"wall_bounce": $"wall_bounce",
}

func _ready():
	connect("score", $"/root/game", "_on_score")
	connect("score", $"/root/game/arena", "_on_score")
	# always starts in center with random direction
	reset_position()
	reset_direction()

# put ball in the dead center of the screen
func reset_position():
	position.x = (settings.court.size.x / 2) - (ball_size.x / 2)
	position.y = (settings.court.size.y / 2) - (ball_size.y / 2)

# pick a semi-random direction for the ball to start moving in
func reset_direction():
	MOVE_DIR = Vector2(rand_range(-1.0,1.0), rand_range(-1.0,1.0)).normalized()
	MOVE_DIR.x = -1.0 if MOVE_DIR.x <= 0 else 1.0
	# there is a statisically insignificantly greater
	# chance that p1 will "get the serve" than p2.
	# this is somewhat balanced by the chance that the serve
	# will go into p1's unreachable margin.
	# the unpredictability of this game means that configurable
	# scores and/or series length are important for balance.
	# probably would be better to refactor this into an actual
	# "coin toss", buuuuuut...eeeeeeh...

# --- PONG PHYSICS --- #
# (we like to move it move it)
func _physics_process(delta):
	move_and_slide(MOVE_DIR * SPEED)
	var collisions_processed = []
	for i in range(0, get_slide_count()):
		var collision = get_slide_collision(i)
		var object = collision.get_collider()
		var collider_id = collision.get_collider_id()
		
		# other situations may call for different handling,
		# but in this one we opt to only process each collision
		# object once per physics tick because that's simpler.
		if collisions_processed.has(collider_id):
			continue
		else:
			collisions_processed.append(collider_id)
		
		if object.is_in_group("wall"):
			# bounce off wall, which is always equivalent to flipping y direction
			sounds.wall_bounce.play()
			MOVE_DIR.y = -MOVE_DIR.y
			
		elif object.is_in_group("paddle"):
			# play that sweet sound
			sounds.paddle_bounce.play()
			
			# lemme tell ya, this collision-handling is some of the
			# most bug-prone code i've ever seen, much less written.
			# and i wrote it.
			# not handling the short edges specifically 
			# causes issues. not considering _which_ long edge
			# paddle you've hit before reflecting x causes issues,
			# at least, it does if you're not handling the short
			# edges properly. also note that every friggin' character
			# in those conditions has to be exactly correct: flipping
			# the comparator breaks things, using y instead of x or
			# vice versa breaks things, and neglecting order-of-operations
			# breaks things.
			# lets break down the actual logic just a bit.
			# if we hit a long edge, reflect x and set y based on 
			# contact point.
			# if we haven't hit a long edge, we've hit a short edge.
			# only one condition _truly_ needs to be expressed, and
			# i ended up going with (not sure if this is the cleanest
			# solution but whatever) is "did i hit a short edge".
			# there are two conditions for the short edge:
			# 1) the ball has moved past the long edge
			# 2) the ball has _not_ moved past the short edge
			# direction of movement is important, as is your 
			# coordinate system.
			# i chose to calculate "edge boundaries" using the center of the
			# ball, which eliminates the concern of figuring out "which edge 
			# vs which face based on which side of the arena". you might
			# think that that's the end of it, since the actual collisions
			# of the actual physics system prevent a lot of potentially-nasty
			# cases from ever occurring, but there was one odd corner case
			# (pun actually not intended, as you will see) that remained.
			# in this way of doing things, the game will treat a ball hitting
			# the short edge of the paddle as a long edge hit if the center
			# hasn't gone past the long edge, which in my mind is like
			# hitting it with the corner of the paddle. it actually makes
			# the game a bit more forgiving with respect to where you can 
			# "successfully" hit the ball with the paddle, and i think it even 
			# makes sense visually to the player. but if we're going with the
			# "reflect x on long edge hit" approach, that leaves a sweet spot 
			# on the back of the paddle where the player can hit the ball
			# successfully, whereas the spot in the middle is considered a
			# short edge and will merely y-reflect (pretty much always resulting
			# in a failed block and a point for the opponent). this doesn't
			# make any sense at all, it's like saying that hitting the ball
			# towards yourself (using the "wrong" side of the paddle) is a
			# "valid serve". this can be fixed by checking that the relative
			# x-position of the ball to the paddle at the point of contact
			# is in the same direction as the ball's x-velocity.
			# ...even after writing the above explanation, i still had
			# to figure out, "ok what happens if it *actually* hits the
			# back side of the paddle, like say it magically bounces off
			# the goal"
			# ...and rewrite the logic _AGAIN_
			# ...kids, 80% of your time is spent on 20% of your code,
			# and that's ok
			
			# knowing that the position property of both paddle and ball
			# denotes their upper-left corner, find the vector from the
			# center of the paddle to the center of the ball.
			var pos = Vector2(position.x + (ball_size.x / 2), position.y + (ball_size.y / 2))
			var paddle_pos = Vector2(object.position.x + (object.paddle_size.x / 2), object.position.y + (object.paddle_size.y / 2))
			# A - B is "the vector from B to A"
			pos = pos - paddle_pos
			
			# "average case" for short-edge collisions
			# check whether the center of the ball has contacted
			# is past the long edge of the paddle. never true in
			# actual long-edge collisions, but also not true if
			# if you hit one of the back corners of the paddle.
			# or in the case where you make a long-edge collision
			# with the "wrong" side of the paddle.
			if abs(pos.x) < (object.paddle_size.x / 2):
				MOVE_DIR.y = -MOVE_DIR.y
			# long-edge collisions
			# remember that "not past_long_edge" is now implied
			# this is basically saying "if we're not past the long edge,
			# and travelling in the 'right' direction, treat is as a long edge"
			elif not MOVE_DIR.x * pos.x > 0:
				MOVE_DIR.x = -MOVE_DIR.x
				MOVE_DIR.y = pos.y * 2 / object.paddle_size.y
			# "corner case", which should be everything else by definition
			else:
				MOVE_DIR.y = -MOVE_DIR.y
			# note that the specific ordering of cases handles priority
			# correctly, as well as the strange case of the "wrong" side
			# (i.e. the "back" side) of the paddle getting hit, which would
			# only ever happen if the ball spawned behind the paddle or
			# bounced off the goal
			
		elif object.is_in_group("goal"):
			# player scores and ball exits play
			var against_player = \
				settings.player.p1 \
				if object.get_name() == "leftgoal" \
				else settings.player.p2
			get_parent().remove_child(self)
			queue_free()
			emit_signal("score", against_player)
