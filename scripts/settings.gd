extends Node2D

enum player { p1, p2 }

# lotta hard coded stuff in here.
# it's my first game, plz cut some slack.
var court = {
	"size": {
		"x": 320,
		"y": 180
	},
	"margin": {
		"x": 10,
		"y": 19
	}
}

# this may need to be tuned, but if "speed" is px / sec,
# then having the ball cross from one side of the arena
# to the other in about 2 sec seems like a sensible default,
# as does having the paddles move about twice as fast as the ball
var speed_base = (court.size.x - (court.margin.x * 2)) / 2
var ball_speed = speed_base * 0.85 # eh...gonna slow it down a bit
var paddle_speed = speed_base * 2

# menu settings
var menu_messages = {
	"top": {
		"welcome": "      WELCOME!       ",
		"p1_wins": "      P1 WINS!       ",
		"p2_wins": "      P2 WINS!       ",
		"paused": "       PAUSED        ",
		"breather": "      BREATHER       ",
	},
	"bottom": {
		"play": "        PLAY         ",
		"next_round": "     NEXT ROUND      ",
		"continue": "      CONTINUE       ",
		"play_again": "     PLAY AGAIN      ",
	},
}

# gameplay settings that don't change
const MIN_WIN_MARGIN = 1
const MAX_WIN_MARGIN = 99
const MIN_SCORE_LIMIT = 1
const MAX_SCORE_LIMIT = 99
const MIN_SERIES_LENGTH = 1
const MAX_SERIES_LENGTH = 21
const SERIES_LENGTH_STEP = 2

# configurables, exposed to the user via the menu
# hard-coded values here are just "sensible defaults"
var SCORE_LIMIT = 10
var SERIES_LENGTH = 3
var WIN_MARGIN = 1

# format setting numbers for display in their menu spots
func get_display_number(num):
	if num >= 10:
		return str(num)
	else:
		return "0" + str(num)

func _ready():
	randomize()