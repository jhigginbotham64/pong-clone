extends Node2D

onready var ui = preload("res://scenes/ui.tscn")

var p1_score = 0
var p1_wins = 0
var p2_score = 0
var p2_wins = 0

var pause_menu_open = false

func update_ui():
	var p1_wins_ui = $"ui/p1_wins/p1_winnum"
	var p2_wins_ui = $"ui/p2_wins/p2_winnum"
	var p1_score_ui = $"ui/p1_score"
	var p2_score_ui = $"ui/p2_score"
	p1_wins_ui.text = str(p1_wins)
	p2_wins_ui.text = str(p2_wins)
	p1_score_ui.text = str(p1_score)
	p2_score_ui.text = str(p2_score)

func reset_match():
	p1_score = 0
	p2_score = 0
	$"arena".reset_match()
	update_ui()

func reset_series():
	p1_wins = 0
	p2_wins = 0
	reset_match()

func _on_score_limit_down_button_clicked():
	if settings.SCORE_LIMIT - 1 >= settings.MIN_SCORE_LIMIT:
		settings.SCORE_LIMIT -= 1
		$"ui/menu/score_limit_display".text = settings.get_display_number(settings.SCORE_LIMIT)

func _on_score_limit_up_button_clicked():
	if settings.SCORE_LIMIT + 1 <= settings.MAX_SCORE_LIMIT:
		settings.SCORE_LIMIT += 1
		$"ui/menu/score_limit_display".text = settings.get_display_number(settings.SCORE_LIMIT)

func _on_win_margin_down_button_button_clicked():
	if settings.WIN_MARGIN - 1 >= settings.MIN_WIN_MARGIN:
		settings.WIN_MARGIN -= 1
		$"ui/menu/win_margin_display".text = settings.get_display_number(settings.WIN_MARGIN)

func _on_win_margin_up_button_clicked():
	if settings.WIN_MARGIN + 1 <= settings.MAX_WIN_MARGIN:
		settings.WIN_MARGIN += 1
		$"ui/menu/win_margin_display".text = settings.get_display_number(settings.WIN_MARGIN)

func _on_series_length_down_button_clicked():
	if settings.SERIES_LENGTH - settings.SERIES_LENGTH_STEP >= settings.MIN_SERIES_LENGTH:
		settings.SERIES_LENGTH -= settings.SERIES_LENGTH_STEP
		$"ui/menu/series_length_display".text = settings.get_display_number(settings.SERIES_LENGTH)

func _on_series_length_up_button_clicked():
	if settings.SERIES_LENGTH + settings.SERIES_LENGTH_STEP <= settings.MAX_SERIES_LENGTH:
		settings.SERIES_LENGTH += settings.SERIES_LENGTH_STEP
		$"ui/menu/series_length_display".text = settings.get_display_number(settings.SERIES_LENGTH)

func _on_reset_match_label_clicked():
	reset_match()

func _on_reset_series_label_clicked():
	reset_series()

func _on_bottom_msg_clicked():
	close_pause_menu()

func _ready():
	# this renders the ui on top of the arena,
	# which requires the ball to have a non-zero z index
	# to show up in front of the player score displays,
	# and for ball visibility to be toggled whenever
	# the menu comes onscreen because you can't adjust
	# the z index of control nodes...
	# except that the ball being invisible is completely 
	# asinine and unnacceptable.
	# but there is no other easy solution, because Godot
	# for all its wonder seems to consider these sorts
	# of things to be corner-cases.
	# so, uh, toggle ball visibility iff area of ball
	# is completely within area of menu. this is stupid.
	var new_ui = ui.instance()
	add_child(new_ui)
	open_pause_menu(settings.menu_messages.top.welcome, settings.menu_messages.bottom.play)

func open_pause_menu(top_msg, bottom_msg):
	var menu = $"ui/menu"
	menu.show()
	menu.set_process(true)
	$"ui/menu/top_msg".text = top_msg
	$"ui/menu/bottom_msg".text = bottom_msg
	$"arena".hide_ball_if_in_menu()
	pause_menu_open = true
	get_tree().set_pause(true)

func close_pause_menu():
	var menu = $"ui/menu"
	menu.hide()
	menu.set_process(false)
	$"arena".show_ball()
	pause_menu_open = false
	get_tree().set_pause(false)

func _input(event):
	if not event.is_echo():
		# spacebar pauses
		# but since i don't want spaghetti in my code, we're
		# going to say that it _does not_ unpause, you have to
		# click the bottom_msg ("PLAY") button for that
		if event.is_action_pressed("ui_select") and not pause_menu_open:
			open_pause_menu(settings.menu_messages.top.paused, settings.menu_messages.bottom["continue"])
		# esc closes the game
		if event.is_action_pressed("ui_cancel"):
			get_tree().quit()

func _on_score(against_player):
	var wins_needed = (settings.SERIES_LENGTH + 1) / 2
	var p1_score_needed = max(p2_score + settings.WIN_MARGIN, settings.SCORE_LIMIT)
	var p2_score_needed = max(p1_score + settings.WIN_MARGIN, settings.SCORE_LIMIT)
	if against_player == settings.player.p1:
		p2_score += 1
		if p2_score >= p2_score_needed:
			p2_wins += 1
	elif against_player == settings.player.p2:
		p1_score += 1
		if p1_score >= p1_score_needed:
			p1_wins += 1
	
	if p1_wins >= wins_needed or p2_wins >= wins_needed:
		# display win message on menu and get ready for next game
		if p1_wins >= wins_needed:
			open_pause_menu(settings.menu_messages.top.p1_wins, settings.menu_messages.bottom.play_again)
		else:
			open_pause_menu(settings.menu_messages.top.p2_wins, settings.menu_messages.bottom.play_again)
		reset_series()
	elif p1_score >= p1_score_needed or p2_score >= p2_score_needed:
		# display pause menu to give players a "breather"
		# so they don't get slaughtered by the ball "spawn rate"
		reset_match()
		open_pause_menu(settings.menu_messages.top.breather, settings.menu_messages.bottom.next_round)
	else:
		update_ui()