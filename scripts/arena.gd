extends Node2D

onready var score = $"score"
onready var ball = preload("res://scenes/ball.tscn")

var current_ball

func reset_match():
	$"p1".reset_position()
	$"p2".reset_position()
	current_ball.reset_position()
	current_ball.reset_direction()

func add_new_ball():
	current_ball = ball.instance()
	add_child(current_ball)
	if $"..".pause_menu_open:
		hide_ball_if_in_menu()

func hide_ball_if_in_menu():
	# SO MANY MAGIC NUMBERS IN THIS CODE
	# ...at least i don't change them much
	var ball_rad_x = current_ball.ball_size.x / 2
	var ball_rad_y = current_ball.ball_size.y / 2
	var ball_center_x = current_ball.position.x + ball_rad_x
	var ball_center_y = current_ball.position.y + ball_rad_y
	var menu_left_bound = 85 + ball_rad_x
	var menu_right_bound = 85 + 147 - ball_rad_x
	var menu_upper_bound = 40 + ball_rad_y
	var menu_lower_bound = 40 + 99 - ball_rad_y
	if ball_center_x > menu_left_bound and ball_center_x < menu_right_bound and \
		ball_center_y > menu_upper_bound and ball_center_y < menu_lower_bound:
		# shouldn't have to deal with set_process, because this
		# function is only called when the arena is paused
		current_ball.hide()

func show_ball():
	current_ball.show()

func _ready():
	add_new_ball()

func _on_score(against_player):
	score.play()
	# this works because it knows that the ball removes/deletes itself
	add_new_ball()
	