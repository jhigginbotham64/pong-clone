extends KinematicBody2D

var SPEED = settings.paddle_speed

# "sensible" default value, used in testing 
export var PLAYER = settings.player.p1

# it appears that ColorRect's rect_position indicates the top-left corner
# EDIT: that is an artifactual comment
var TOP_LIMIT = settings.court.margin.y

onready var paddle_size = {
	"x": $"paddle_shape".get_shape().get_extents().x * 2,
	"y": $"paddle_shape".get_shape().get_extents().y * 2,
}

onready var BOTTOM_LIMIT = settings.court.size.y - settings.court.margin.y - paddle_size.y

var MOVE_DIR = Vector2()

func _input(event):
	# dontcha love boolean expressions?
	# tl;dr go up if going up, go down if going down
	# p1 uses wasd, p2 uses the arrow keys
	if event is InputEventKey and not event.echo:
		# going up
		if ( \
			(PLAYER == settings.player.p1 and \
				((event.pressed and event.scancode == KEY_W) or \
				(not event.pressed and event.scancode == KEY_S))) or \
			(PLAYER == settings.player.p2 and \
				((event.pressed and event.scancode == KEY_UP) or \
				(not event.pressed and event.scancode == KEY_DOWN)))):
			MOVE_DIR.y -= 1
		# going down
		if ( \
			(PLAYER == settings.player.p1 and \
				((event.pressed and event.scancode == KEY_S) or \
				(not event.pressed and event.scancode == KEY_W))) or \
			(PLAYER == settings.player.p2 and \
				((event.pressed and event.scancode == KEY_DOWN) or \
				(not event.pressed and event.scancode == KEY_UP)))):
			MOVE_DIR.y += 1

func _physics_process(delta):
	move_and_slide(SPEED * MOVE_DIR)
	position.y = max(position.y, TOP_LIMIT)
	position.y = min(position.y, BOTTOM_LIMIT)

func reset_position():
	position.y = (settings.court.size.y - paddle_size.y) / 2
	if PLAYER == settings.player.p1:
		position.x = settings.court.margin.x
	elif PLAYER == settings.player.p2:
		position.x = settings.court.size.x - settings.court.margin.x - paddle_size.x

func _ready():
	reset_position()