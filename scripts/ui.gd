extends Control


# thanks obama (9/2/2019, issue is yet unresolved)
# https://github.com/godotengine/godot/issues/16854


signal score_limit_down_button()
signal score_limit_up_button()
signal win_margin_down_button()
signal win_margin_up_button()
signal series_length_down_button()
signal series_length_up_button()
signal reset_match_label()
signal reset_series_label()
signal bottom_msg()


onready var menu_rows = {
	"score_limit": "score_limit",
	"win_margin": "win_margin",
	"series_length": "series_length",
	"reset_match": "reset_match",
	"reset_series": "reset_series",
	"bottom_msg": "bottom_msg"
}


onready var menu_row_items = {
	"score_limit": {
		"label": "score_limit_label",
		"down_button": "score_limit_down_button",
		"display": "score_limit_display",
		"up_button": "score_limit_up_button",
		"endspace": "score_limit_endspace"
	},
	"win_margin": {
		"label": "win_margin_label",
		"down_button": "win_margin_down_button",
		"display": "win_margin_display",
		"up_button": "win_margin_up_button",
		"endspace": "win_margin_endspace"
	},
	"series_length": {
		"label": "series_length_label",
		"down_button": "series_length_down_button",
		"display": "series_length_display",
		"up_button": "series_length_up_button",
		"endspace": "series_length_endspace"
	},
	"reset_match": {
		"label": "reset_match_label",
	},
	"reset_series": {
		"label": "reset_series_label",
	},
	"bottom_msg": {
		"bottom_msg": "bottom_msg"	
	}
}


func _ready():
	$"menu/score_limit_display".text = settings.get_display_number(settings.SCORE_LIMIT)
	$"menu/win_margin_display".text = settings.get_display_number(settings.WIN_MARGIN)
	$"menu/series_length_display".text = settings.get_display_number(settings.SERIES_LENGTH)
	var game = $"/root/game"
	connect("score_limit_down_button", game, "_on_score_limit_down_button_clicked")
	connect("score_limit_up_button", game, "_on_score_limit_up_button_clicked")
	connect("win_margin_down_button", game, "_on_win_margin_down_button_clicked")
	connect("win_margin_up_button", game, "_on_win_margin_up_button_clicked")
	connect("series_length_down_button", game, "_on_series_length_down_button_clicked")
	connect("series_length_up_button", game, "_on_series_length_up_button_clicked")
	connect("reset_match_label", game, "_on_reset_match_label_clicked")
	connect("reset_series_label", game, "_on_reset_series_label_clicked")
	connect("bottom_msg", game, "_on_bottom_msg_clicked")


var cur_row = ""


func handle_mouse_input(event, row, node_name):
	if row in menu_rows.keys():
		# (re)color row
		for key in menu_row_items[row].keys():
			if key != "display":
				var n = get_node("menu/" + menu_row_items[row][key]) # save some typing
				if n.name == node_name and \
					(node_name.ends_with("button") or node_name in \
					["reset_match_label","reset_series_label","bottom_msg"]):
					n.color = n.alt_color if event.is_action_pressed("left_click") else n.button_hover_color
					if event.is_action_released("left_click") and not event.is_echo():
						emit_signal(node_name)
				else:
					n.color = n.alt_color
		# uncolor old row		
		if cur_row in menu_rows.keys() and cur_row != row:
			for key in menu_row_items[cur_row].keys():
				if key != "display":
					var n = get_node("menu/" + menu_row_items[cur_row][key])
					n.color = n.initial_color
		cur_row = row


func _on_score_limit_down_button_gui_input(event):
	handle_mouse_input(event, menu_rows.score_limit, menu_row_items.score_limit.down_button)


func _on_score_limit_up_button_gui_input(event):
	handle_mouse_input(event, menu_rows.score_limit, menu_row_items.score_limit.up_button)


func _on_win_margin_down_button_gui_input(event):
	handle_mouse_input(event, menu_rows.win_margin, menu_row_items.win_margin.down_button)


func _on_win_margin_up_button_gui_input(event):
	handle_mouse_input(event, menu_rows.win_margin, menu_row_items.win_margin.up_button)


func _on_series_length_down_button_gui_input(event):
	handle_mouse_input(event, menu_rows.series_length, menu_row_items.series_length.down_button)


func _on_series_length_up_button_gui_input(event):
	handle_mouse_input(event, menu_rows.series_length, menu_row_items.series_length.up_button)


func _on_reset_match_label_gui_input(event):
	handle_mouse_input(event, menu_rows.reset_match, menu_row_items.reset_match.label)


func _on_reset_series_label_gui_input(event):
	handle_mouse_input(event, menu_rows.reset_series, menu_row_items.reset_series.label)


func _on_bottom_msg_gui_input(event):
	handle_mouse_input(event, menu_rows.bottom_msg, menu_row_items.bottom_msg.bottom_msg)


func _on_score_limit_label_gui_input(event):
	handle_mouse_input(event, menu_rows.score_limit, menu_row_items.score_limit.label)


func _on_score_limit_display_gui_input(event):
	handle_mouse_input(event, menu_rows.score_limit, menu_row_items.score_limit.display)


func _on_score_limit_endspace_gui_input(event):
	handle_mouse_input(event, menu_rows.score_limit, menu_row_items.score_limit.endspace)


func _on_win_margin_label_gui_input(event):
	handle_mouse_input(event, menu_rows.win_margin, menu_row_items.win_margin.label)


func _on_win_margin_display_gui_input(event):
	handle_mouse_input(event, menu_rows.win_margin, menu_row_items.win_margin.display)


func _on_win_margin_endspace_gui_input(event):
	handle_mouse_input(event, menu_rows.win_margin, menu_row_items.win_margin.endspace)


func _on_series_length_label_gui_input(event):
	handle_mouse_input(event, menu_rows.series_length, menu_row_items.series_length.label)


func _on_series_length_display_gui_input(event):
	handle_mouse_input(event, menu_rows.series_length, menu_row_items.series_length.display)


func _on_series_length_endspace_gui_input(event):
	handle_mouse_input(event, menu_rows.series_length, menu_row_items.series_length.endspace)
