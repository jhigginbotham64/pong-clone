extends Control

# the idea is that this scene renders cellphone characters
# in either black or white, and that re-assigning either
# the color or text variable updates the rendered string
# if you look at this code and you're like
# "this is too terrible to be true"
# don't worry that was my reaction too

# freakin...having to set these manually because I don't know an easier way...
const CHAR_WIDTH = 7
const CHAR_HEIGHT = 9

onready var white_cellphone_char = preload("res://scenes/white_cellphone_char.tscn")
onready var black_cellphone_char = preload("res://scenes/black_cellphone_char.tscn")

enum colors { WHITE_ON_BLACK, BLACK_ON_WHITE, GREY_ON_BLACK, BLACK_ON_GREY }
export var color = -1
var internal_color = color
export var text = ""
var internal_text = text

# these defs were designed for, and are only used by, menu items in ui.gd
# they are completely static, and set in _ready
var initial_color = -1
var alt_color = -1
var button_hover_color = -1

const ascii_cellphone_frame_map = {
	" ": 0,
	"!": 1,
	"\"": 2,
	"#": 3,
	"$": 4,
	"%": 5,
	"&": 6,
	"'": 7,
	"(": 8,
	")": 9,
	"*": 10,
	"+": 11,
	",": 12,
	"-": 13,
	".": 14,
	"/": 15,
	"0": 16,
	"1": 17,
	"2": 18,
	"3": 19,
	"4": 20,
	"5": 21,
	"6": 22,
	"7": 23,
	"8": 24,
	"9": 25,
	":": 26,
	";": 27,
	"<": 28,
	"=": 29,
	">": 30,
	"?": 31,
	"@": 32,
	"A": 33,
	"B": 34,
	"C": 35,
	"D": 36,
	"E": 37,
	"F": 38,
	"G": 39,
	"H": 40,
	"I": 41,
	"J": 42,
	"K": 43,
	"L": 44,
	"M": 45,
	"N": 46,
	"O": 47,
	"P": 48,
	"Q": 49,
	"R": 50,
	"S": 51,
	"T": 52,
	"U": 53,
	"V": 54,
	"W": 55,
	"X": 56,
	"Y": 57,
	"Z": 58,
	"[": 59,
	"\\": 60,
	"]": 61,
	"^": 62,
	"_": 63,
	"`": 64,
	"a": 65,
	"b": 66,
	"c": 67,
	"d": 68,
	"e": 69,
	"f": 70,
	"g": 71,
	"h": 72,
	"i": 73,
	"j": 74,
	"k": 75,
	"l": 76,
	"m": 77,
	"n": 78,
	"o": 79,
	"p": 80,
	"q": 81,
	"r": 82,
	"s": 83,
	"t": 84,
	"u": 85,
	"v": 86,
	"w": 87,
	"x": 88,
	"y": 89,
	"z": 90,
	"{": 91,
	"|": 92,
	"}": 93,
	"~": 94,
}

func _ready():
	if color == colors.WHITE_ON_BLACK or color == colors.BLACK_ON_WHITE:
		initial_color = color
		alt_color = initial_color + 2
		button_hover_color = alt_color ^ 1 # yeeeeeeeah bitwise XOR

func _process(delta):
	# render text in given color
	if text != internal_text or color != internal_color:
		internal_text = text
		internal_color = color
		# BUG: i hate to say this, and don't say it normally, but...
		# i am admitting defeat against a weird bug,
		# it seems that rect_scale > Vector2(1,1) on
		# the player scores messes with their mouse detection,
		# where not multiplying by rect_scale disables
		# mouse detection, but multiplying rect_scale causes
		# the detection rect to be much larger than it should
		# be as well as in the wrong position (as in, seeming
		# to start from the bottom-right corner of the string.
		# i'd love to understand it better, but...the normal
		# case (rect_scale = Vector2(1,1) works _perfectly_,
		# and that's my only actual use case for mouse detection
		# on cellphone_strings.
		rect_size.x = internal_text.length() * 7# * rect_scale.x
		rect_size.y = 9# * rect_scale.y
		for child in get_children():
			if child is Sprite or child is ColorRect: 
				remove_child(child)
				child.queue_free()
		var new_char_scene = white_cellphone_char if \
			color == colors.WHITE_ON_BLACK or color == colors.BLACK_ON_GREY \
			else black_cellphone_char
		for i in range(0,text.length()):
			var new_char = new_char_scene.instance()
			new_char.position.x += i * CHAR_WIDTH
			new_char.frame = ascii_cellphone_frame_map[text[i]]
			
			if color == colors.BLACK_ON_GREY or color == colors.GREY_ON_BLACK:
				# do i know an easier way to do this?
				# no, of course not.
				# create a grey background for the sprite,
				# then give the sprite a CanvasItemMaterial
				# with blend mode set to subtract, this effectively
				# converts a black-on-white sprite to grey-on-black
				# and a white-on-black sprite to black-on-grey.
				# i use the grey-on-black for score counters.
				var grey_background = ColorRect.new()
				grey_background.color = Color(0.5,0.5,0.5,1.0)
				grey_background.rect_size = Vector2(7,9)
				grey_background.rect_position.x += i * CHAR_WIDTH
				grey_background.mouse_filter = MOUSE_FILTER_IGNORE
				add_child(grey_background)
				new_char.material = CanvasItemMaterial.new()
				new_char.material.blend_mode = BLEND_MODE_SUB
			add_child(new_char)