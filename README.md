# pong-clone

My attempt at the aspiring game developer's age-old rite-of-passage, the "pong clone". 

Built using the [Godot Engine](https://godotengine.org/).



## Credits

Everything under /assets was made by someone else and put in the Public Domain (CC0) on [OpenGameArt.org](https://opengameart.org/):

Sounds by captaincrunch80: [3 Ping Pong Sounds 8-Bit Style](https://opengameart.org/content/3-ping-pong-sounds-8-bit-style)

"CELLPHONE" font by domsson: [ASCII Bitmap Font "CELLPHONE"](https://opengameart.org/content/ascii-bitmap-font-cellphone)

Everything else, of course, is by me. Except the game design, I didn't invent Pong.
